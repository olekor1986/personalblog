<?php


class DatabaseTest extends Model
{
    public static function createPortfolioTable(PDO $db)
    {
        try {
            $sql = 'CREATE TABLE portfolio 
        (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            title VARCHAR(255),
            year VARCHAR(255),
            url VARCHAR(255),
            description VARCHAR(255)
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
            $db->exec($sql);
        } catch (Exception $e) {
            $message = 'Ошибка создания таблицы Portfolio! ' . $e->getMessage();
            die($message);
        }

    }
    public static function createArticlesTable(PDO $db)
    {
        try {
            $sql = 'CREATE TABLE articles 
        (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            title VARCHAR(255),
            text VARCHAR(1500)
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
            $db->exec($sql);
        } catch (Exception $e) {
            $message = 'Ошибка создания таблицы Articles! ' . $e->getMessage();
            die($message);
        }

    }
    public static function addPortfolioToDatabase(PDO $db, array $array)
    {
        try {
            foreach ($array as $value) {
                $sql = 'INSERT INTO portfolio SET
                title = :title,
                year = :year,
                url = :url,
                description = :description';
                $seederObject = $db->prepare($sql);
                $seederObject->bindValue(':title', $value['title']);
                $seederObject->bindValue(':year', $value['year']);
                $seederObject->bindValue(':url', $value['url']);
                $seederObject->bindValue(':description', $value['description']);
                $seederObject->execute();
            }
        } catch (Exception $e) {
            $message = 'Error adding Portfolio!' . $e->getMessage();
            die($message);
        }
    }
    public static function addArticlesToDatabase(PDO $db, array $array)
    {
        try {
            foreach ($array as $value) {
                $sql = 'INSERT INTO articles SET
                title = :title,
                text = :text';
                $seederObject = $db->prepare($sql);
                $seederObject->bindValue(':title', $value['title']);
                $seederObject->bindValue(':text', $value['text']);
                $seederObject->execute();
            }
        } catch (Exception $e) {
            $message = 'Error adding Articles!' . $e->getMessage();
            die($message);
        }
    }
}