<?php
require_once '../application/core/Model.php';
require_once 'Classes/DatabaseTest.php';

$db = new Model();

DatabaseTest::createArticlesTable($db->getDb());
DatabaseTest::createPortfolioTable($db->getDb());

header("Location:../index.php");
?>