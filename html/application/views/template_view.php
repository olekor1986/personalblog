<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Personalblog</title>
</head>
<body>
<div class="header">
    <ul>
        <li><a href="/">Homepage</a></li>
        <li><a href="/Portfolio">Portfolio</a></li>
        <li><a href="/Articles">Articles</a></li>
        <li><a href="/Page/contacts">Contacts</a></li>
        <li><a href="/Page/about_us">About us</a></li>
    </ul>
</div>
<div class="content">
    <?php include_once 'application/views/' . $contentView . '_view.php'?>
</div>
<div>
    <p>SUPER PERSONAL BLOG (c) 2019</p>
</div>
</body>
</html>