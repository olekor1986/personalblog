<?php


class Model_Portfolio extends Model
{
    public function getAllPortfolio()
    {
        try {
            $sql = 'SELECT * FROM portfolio';
            $result = $this->db->query($sql);
            $portfolio = $result->fetchAll();
            return $portfolio;
        } catch(Exception $e) {
            $message = 'Error getting data! ' . $e->getMessage();
            die($message);
        }
    }
    public function getPortfolioItemById($id)
    {
        try {
            $sql = "SELECT * FROM portfolio WHERE id = :id";
            $selectedItem = $this->db->prepare($sql);
            $selectedItem->bindValue(':id', $id);
            $selectedItem->execute();
            $item = $selectedItem->fetch();
            if (!empty($item)) {
                return $item;
            }
            die('Page not found');
        } catch (Exception $e) {
            $message = 'Error getting article' . $e->getMessage();
            die($message);
        }
    }
}