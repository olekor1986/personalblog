<?php


class Model_Articles extends Model
{
    public function getAllArticles()
    {
        try {
            $sql = 'SELECT * FROM articles';
            $result = $this->getDb()->query($sql);
            $articles = $result->fetchAll();
            return $articles;
        } catch(Exception $e) {
            $message = 'Error getting data!' . $e->getMessage();
            die($message);
        }
    }
    public function getArticleById($id)
    {
        try {
            $sql = "SELECT * FROM articles WHERE id = :id";
            $selectedArticle = $this->getDb()->prepare($sql);
            $selectedArticle->bindValue(':id', $id);
            $selectedArticle->execute();
            $article = $selectedArticle->fetch();
            if (!empty($article)){
                return $article;
            }
                die('Page not found');
        } catch (Exception $e) {
            $message = 'Error getting article' . $e->getMessage();
            die($message);
        }
    }
}