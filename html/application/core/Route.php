<?php


class Route
{
    public static function start()
    {
        $controllerName = 'Page';
        $modelName = 'Page';
        $actionName = 'index';
        $id = '';
        $route =[];
        if (!empty($_GET['path'])) {
            $path = substr($_GET['path'], 1);
            $route = explode('/', $path);
        }
        if (!empty($route[0])){
            $controllerName = $route[0];
            $modelName = $route[0];
        }
        if (!empty($route[1])){
            $actionName = $route[1];
        }
        if (!empty($route[2])){
            if (preg_match('/^[0-9]/', $route[2])){
                $id = $route[2];
            }
        }

        $controllerName = 'Controller_' . ucfirst(strtolower($controllerName));
        $modelName = 'Model_' . ucfirst(strtolower($modelName));
        $actionName = 'action_' . strtolower($actionName);

        $modelPath = 'application/models/' . $modelName . '.php';
        if (file_exists($modelPath)){
            require_once $modelPath;
        }

        $controllerPath = 'application/controllers/' . $controllerName . '.php';
        if (file_exists($controllerPath)){
            require_once $controllerPath;
        } else {
            die('NOT FOUND');
        }
        if (isset($id)) {
            $controller = new $controllerName($id);
        } else {
            $controller = new $controllerName();
        }

        if (method_exists($controller, $actionName)){
            $controller->$actionName();
        } else {
            die('NOT FOUND');
        }

    }
}