<?php


class Model
{
    protected $db;

    public function __construct()
    {
        try {
            $this->db = new PDO('mysql:host=localhost;dbname=personalblog', 'admin', '11111');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db->exec('SET NAMES "utf8"');
        } catch (Exception $e) {
            $message = 'DB access error! ' . $e->getMessage();
            die($message);
        }
    }

    public function getDb()
    {
        return $this->db;
    }

}