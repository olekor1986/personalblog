<?php


class Controller_Portfolio extends Controller
{
    protected $id;

    public function  __construct($id)
    {
        parent::__construct();
        $this->model = new Model_Portfolio;
        $this->id = $id;
    }

    public function action_index()
    {
        $data = [];
        $data['works'] = $this->model->getAllPortfolio();
        $this->view->generateView('portfolio', 'template_view.php', $data);
    }
    public function action_show()
    {
        $data = [];
        if (isset($this->id)){
            $data['portfolio_item_by_id'] = $this->model->getPortfolioItemById($this->id);
            $this->view->generateView('portfolio_item', 'template_view.php', $data);
        }
    }
}