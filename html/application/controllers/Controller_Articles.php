<?php


class Controller_Articles extends Controller
{
    protected $id;

    public function  __construct($id)
    {
        parent::__construct();
        $this->model = new Model_Articles;
        $this->id = $id;
    }

    public function action_index()
    {
        $data = [];
        $data['articles'] = $this->model->getAllArticles();
        $this->view->generateView('articles', 'template_view.php', $data);
    }

    public function action_show()
    {
        $data = [];
        if (isset($this->id)){
            $data['article_by_id'] = $this->model->getArticleById($this->id);
            $this->view->generateView('article', 'template_view.php', $data);
        }
    }
}
