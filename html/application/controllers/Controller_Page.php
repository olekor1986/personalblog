<?php


class Controller_Page extends Controller
{
    public function action_index()
    {
        $this->view->generateView('homepage', 'template_view.php');
    }
    public function action_contacts()
    {
        $this->view->generateView('contacts', 'template_view.php');

    }
    public function action_about_us()
    {
        $this->view->generateView('about_us', 'template_view.php');
    }
}